# DeathByUSB

DeathByUSB is a proof-of-concept for an advanced, automated, USB-based penetration testing device. When plugged into an unlocked machine, it detects the machine's operating system and then rapidly sends a series of keystrokes to the machine which install a persistent reverse-shell backdoor, allowing the machine to be controlled remotely. The whole process takes less than a minute.

# Automatic Operating System Detection

USB keyboards not only send information to the machine that they're connected to, but they can also receive some information back. For example, when a user presses the caps lock key, the keyboard sends a signal to the machine telling it that the caps lock key has been pressed, and then the machine sends a signal back telling the keyboard that the keyboard's caps lock LED should be updated accordingly. The same is true for num lock and scroll lock, _but only for operating systems which support num lock and scroll lock_.

By pressing the num lock and scroll lock keys and checking to see if the machine sends a signal to update the corresponding LEDs, DeathByUSB can determine whether the machine's operating system supports num lock and scroll lock, and then use this information to guess what the operating system probably is. MacOS supports neither num lock nor scroll lock, _some_ Linux distros support num lock but not scroll lock (others support both), while Windows supports both.

First, DeathByUSB checks if the operating system supports num lock. If not, it's probably MacOS. If so, it then checks if the operating system supports scroll lock. If so, it's probably Linux. If not, it's probably Windows.

# Payloads

## \*Nix (MacOS/Linux) Payload

(TODO)

## Windows Payload

(TODO)

# Usage

DeathByUSB works on the [Teensy](https://www.pjrc.com/teensy/) and [Arduino Leonardo](https://www.arduino.cc/en/Main/Arduino_BoardLeonardo) without any changes to the code. (It probably works on other devices too, but these are the only ones I've tested.) Just make sure to specify which device you're using in the Arduino IDE (under _Tools_ > _Board_). If you're using a Teensy, you'll need to set its USB type to one which includes "Keyboard" (_Tools_ > _USB Type_). If you're using an Arduino, you'll need to include the [HID-Project library](https://www.arduino.cc/reference/en/libraries/hid-project/) (_Tools_ > _Manage Libraries..._).

You'll need to set the HOST and PORT at the top of the code to the host and port of your listener (where the reverse shell should connect back). You can use a program like Netcat or Metasploit to catch the shell.

# Prevention

The most straightforward way to prevent DeathByUSB from being used against you is to lock your machine when unattended and don't plug in unfamiliar devices. More extreme measures might include using software to block unknown USB devices, or disabling your USB ports.

# Possible Areas for Future Research

- Mouse/touchscreen emulation
- Mass storage device emulation
- Emulating other devices, like an ethernet adapter or external display (this would require more sophisticated hardware)
- Using keyboard LEDs to get feedback from running programs
- Staged payloads
- Data exfiltration

# Credits

DeathByUSB was inspired by these projects:

- [USB Rubber Ducky](https://shop.hak5.org/products/usb-rubber-ducky-deluxe)
- [USBdriveby: exploiting USB in style](https://samy.pl/usbdriveby/)
- [The Peensy – Reliable Penetration Testing Payloads using a Teensy](https://www.offensive-security.com/offsec/advanced-teensy-penetration-testing-payloads/)
- [The Social-Engineer Toolkit](https://www.trustedsec.com/social-engineer-toolkit-set/)
- [Root a Mac in 10 seconds or less](http://patrickmosca.com/root-a-mac-in-10-seconds-or-less/)
